import serial
from datetime import datetime

PORT = '/dev/serial0'

def main(data):
    # decode byte data to string and split with ','
    split_data = data.decode('utf-8').split(',')
    
    if split_data[0] == '$GPRMC':
        if split_data[2] == 'V':
            print('No satellite data')
            return
        
        print('DateTime: ' + str(datetime.now()))
        print('Latitude: ' + convert_to_decimal_degree(split_data[3],split_data[4]))
        print('Longtitude: ' + convert_to_decimal_degree(split_data[5],split_data[6]))
        print('Speed: ' + split_data[7])

def convert_to_decimal_degree(coordinate, direction):
    multiplier = 1 if direction in ['N', 'E'] else -1
    degrees = float(coordinate) // 100

    minutes = float(coordinate) - 100*degrees

    return str(round(multiplier * (degrees + minutes/60),4))

if __name__ == '__main__':

    ser = serial.Serial(PORT, baudrate = 9600, timeout = 0.5)
    while True:
        data = ser.readline()
        main(data)

