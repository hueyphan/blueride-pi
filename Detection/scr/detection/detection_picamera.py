import serial
import argparse
import io

from datetime import datetime
from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
import cv2
import time
import numpy as np
import picamera

PORT = '/dev/serial0'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--model','-m', required=True,
            help='Detection SSD model path.')
    parser.add_argument('--label','-l', required=False,
            help='Labels file path.')
    args = parser.parse_args()
    
    # Initialize engine
    print('[INFO]Loading model...')
    engine = DetectionEngine(args.model)
    #labels = dataset_utils.read_label_file(args.label)

    # Initialize from serial port
    ser = serial.Serial(PORT, baudrate = 9600, timeout = 0.5)
    print('[INFO]Loading camera...')
    with picamera.PiCamera() as camera:
        camera.resolution = (300,300)
        camera.framerate = 30
        _, height, width, _ = engine.get_input_tensor_shape()
        camera.start_preview()
        time.sleep(2.0)
        try:
            stream = io.BytesIO()
            for _ in camera.capture_continuous(stream, format='rgb',
                use_video_port = True, resize=(width, height)):
                stream.truncate()
                stream.seek(0)
                input_tensor = np.frombuffer(stream.getvalue(), dtype=np.uint8)
                start_ms = time.time()
                results = engine.detect_with_input_tensor(input_tensor, top_k=1)
                #results = engine.detect_with_image(input_tensor,threshold=0.05,
                        #keep_aspect_ratio=True, relative_coord=False,top_k=10)
                elapsed_ms = time.time() - start_ms

                for r in results:
                    camera.annotate_text = '%s %.2f\n%.2fms' % (
                            labels[r.label_id], r.score, elapsed_ms * 1000.0)
                # Read data from serial port 
                data = ser.readline()

                # Decode byte data to string and split with ','
                get_data_from_GPS(data.decode('utf-8').split(','))
            
        finally:
            # Release the capture
            camera.stop_preview()

# Get data from GPS after split
def get_data_from_GPS(data):
    if data[0] == '$GPRMC':
        if data[2] == 'V':
            print('No satellite data')
            return
        
       # print('DateTime: ' + str(datetime.now()))
       # print('Latitude: ' + convert_to_decimal_degree(data[3],data[4]))
       # print('Longtitude: ' + convert_to_decimal_degree(data[5],data[6]))
       # print('Speed: ' + data[7])

# Convert NMEA $GPRMC data to decimal degree
def convert_to_decimal_degree(coordinate, direction):
    multiplier = 1 if direction in ['N', 'E'] else -1
    
    degrees = float(coordinate) // 100
    minutes = float(coordinate) - 100*degrees

    return str(round(multiplier * (degrees + minutes/60),4))

def coral():
    return

if __name__ == '__main__':
    main()

