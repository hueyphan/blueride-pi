from imutils.video import FPS
from imutils.object_detection import non_max_suppression

import numpy as np
import cv2
import imutils
import sys, getopt
import time
import datetime
import serial
import csv

# local modules
from video import create_capture
from common import clock, draw_str
from pyimagesearch.centroidtracker import CentroidTracker

PORT = '/dev/serial0'

# Get data from GPS after split
def get_data_from_GPS(data):
    if data[0] == '$GPRMC':
        if data[2] == 'V':
            print('No satellite data')
            return
        
        latitude = convert_to_decimal_degree(data[3],data[4])
        longtitude = convert_to_decimal_degree(data[5],data[6])
        
        return latitude, longtitude

# Convert NMEA $GPRMC data to decimal degree
def convert_to_decimal_degree(coordinate, direction):
    multiplier = 1 if direction in ['N', 'E'] else -1
    
    degrees = float(coordinate) // 100
    minutes = float(coordinate) - 100*degrees

    return str(round(multiplier * (degrees + minutes/60),5))

# Get speed from GPS
def get_speed_from_GPS(data):
    if data[0] == '$GPVTG':
        return str(convert_speed_to_mph(float(data[7])))

# Convert to mph
def convert_speed_to_mph(kmh):
    return kmh/1.609

# Write to CSV
def write_impressions_csv(objId, obj, obj_dict, data_from_GPS, 
        speed_from_GPS, start_time, end_time, writer):

    if objId not in obj_dict: 
        obj_dict[objId] = [objId,(data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Face Detected','1','Null', start_time, end_time,
                'Null','1']

        if objId != 0:
            writer.writerow([objId,(data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Face Detected','1','Null', start_time, 
                start_time + datetime.timedelta(seconds=end_time),
                'Null','1'])

    elif objId in obj_dict:
        temp = [objId,(data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Face Detected','1','Null', start_time, end_time,
                'Null','1']
        obj_dict[objId].append(temp)
        
def write_timeline_csv(objs, label, data_from_GPS, 
        speed_from_GPS, timeline_writer):
    if len(objs) > 0:
        temp = [datetime.datetime.now(), 
                (data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Null', len(objs)]
        timeline_writer.writerow(temp)

# Detect overlap boxes
def non_max_suppression_fast(boxes, overlapThresh):
    try:
        if len(boxes) == 0:
            return []

        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        pick = []

        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]

        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = np.argsort(y2)

        while len(idxs) > 0:
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            overlap = (w * h) / area[idxs[:last]]

            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > overlapThresh)[0])))

        return boxes[pick].astype("int")
    except Exception as e:
        print("Exception occurred in non_max_suppression : {}".format(e))

def detect(frame, cascade):
    rects = cascade.detectMultiScale(frame, scaleFactor=1.3, 
            minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    if len(rects) == 0:
        return []

    rects[:,2:] += rects[:,:2]

    return rects

def draw_rects(frame, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(frame, (x1, y1), (x2, y2), color, 2)

def main():

    try:
        video_src = video_src[0]
    except:
        video_src = 0
    
    # Initialize centroid tracker and frame dimensions
    tracker = CentroidTracker(maxDisappeared=3, maxDistance=50)

    print('[INFO] Loading models...')
    cascade_fn = (r'./data/haarcascades/haarcascade_frontalface_alt.xml')
    nested_fn  = (r'./data/haarcascades/haarcascade_eye.xml')

    cascade = cv2.CascadeClassifier(cascade_fn)
    nested = cv2.CascadeClassifier(nested_fn)

    cam = create_capture(video_src)

    print('[INFO] Warming up camera...')

    # Time to warm up camera
    time.sleep(2.0)
    fps = FPS().start()
    
    # Initialize from serial port
    ser = serial.Serial(PORT, baudrate = 9600, timeout = 0.5)

    # Initialize time tracking
    object_id_list = []
    start_time = dict()
    end_time = dict()

    obj_dict = dict()

    data_from_GPS = ''
    speed_from_GPS = ''
    # Create impressions csv file
    impressions_header = ['Id', 'GPS', 'Speed(mph)', 'Person',
            'Ad Id', 'Ad Position', 'Start Time', 'End Time',
            'Confidence %', 'Unit Id']
    impressions_writer = csv.writer(open(
        r'./CSV/Impressions ' + str(datetime.datetime.now())+'.csv', 'w'))
    impressions_writer.writerow(impressions_header)

    # Create timeline csv file
    timeline_header = ['timestamp', 'GPS', 'Speed(mph)',
            'Impression Present', 'Human in View']
    timeline_writer = csv.writer(open(
        r'./CSV/Timeline ' + str(datetime.datetime.now())+'.csv', 'w'))
    timeline_writer.writerow(timeline_header)

    while True:
        _ret, frame = cam.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)

        # Resize the frame
        gray = imutils.resize(frame, width=min(350, frame.shape[1]))

        t = clock()
        rects = detect(gray, cascade)

        # Overlap boxes
        boundingboxes = np.array(rects)
        boundingboxes = boundingboxes.astype(int)
        rects = non_max_suppression_fast(boundingboxes, 0.3)
        
        detected_objects = tracker.update(rects)

        vis = gray.copy()

        #rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        #pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)
        draw_rects(vis, rects, (0, 255, 0))
        
        #for x1, y1, x2, y2 in pick:
            #cv2.rectangle(vis, (x1, y1), (x2, y2), (0, 255, 0, 2))

        # Create copy of faces and detect eyes then draw eyes
        if not nested.empty():
            for x1, y1, x2, y2 in rects:
                roi = gray[y1:y2, x1:x2]
                vis_roi = vis[y1:y2, x1:x2]
                subrects = detect(roi.copy(), nested)
                draw_rects(vis_roi, subrects, (255, 0, 0))

        # Loop over tracked objects
        for (objectId, centroid) in detected_objects.items():
            (startX, startY, endX, endY) = centroid

            y = startY - 15 if startY - 15 > 15 else startY + 15
            
            # Calculate time in frame of object base on their ID
            if objectId not in object_id_list:
                object_id_list.append(objectId)
                start_time[objectId] = datetime.datetime.now()
                end_time[objectId] = 0
            else:
                curr_time = datetime.datetime.now()
                old_time = start_time[objectId]
                time_diff = curr_time - old_time
                start_time[objectId] = datetime.datetime.now()
                sec = time_diff.total_seconds()
                end_time[objectId] += sec
            
            # Read GPS data from serial port 
            GPS_data = ser.readline()
            try:
                # Decode byte data to string and split with ','
                data_from_GPS = get_data_from_GPS(
                        GPS_data.decode('utf-8').split(','))
                
                # Read speed data, decode and split
                speed_data = ser.readline()
                speed_from_GPS = get_speed_from_GPS(
                        speed_data.decode('utf-8').split(','))
            except(UnicodeDecodeError, AttributeError):
                pass

            if data_from_GPS and speed_from_GPS:
                    write_impressions_csv(objectId, centroid, obj_dict, 
                            data_from_GPS, speed_from_GPS, 
                            start_time[objectId], end_time[objectId], 
                            impressions_writer)
                    
                    time.sleep(1)
                    write_timeline_csv(rects, label, data_from_GPS, 
                            speed_from_GPS, timeline_writer)

            text = "ID {}, {} second".format(
                    objectId, int(end_time[objectId]))
            cv2.putText(vis, text, (startX, y),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)


        dt = clock() - t

        draw_str(vis, (20, 20), 'time: %.1f ms' % (dt*1000))
        cv2.imshow('Frame', vis)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        fps.update()

    fps.stop()
    print('[INFO] Elapsed time: {:.2f}'.format(fps.elapsed()))
    print('[INFO] Approx. FPS: {:.2f}'.format(fps.fps()))
    print('[INFO] Done')

if __name__ == '__main__':
    main()
    cv2.destroyAllWindows()
