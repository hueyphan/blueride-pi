from __future__ import print_function
from imutils.object_detection import non_max_suppression
from imutils import paths
from imutils.video import VideoStream
from imutils.video import FPS
from video import create_capture
from common import clock, draw_str
import numpy as np
import imutils
import cv2
import time

def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4,
            minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

def main():
    # Initialize Hog Descriptor Person detector
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    # Initialize Face Cascade
    face_cascade = (r'./data/haarcascades/haarcascade_frontalface_alt.xml')

    cascade = cv2.CascadeClassifier(face_cascade)

    # Open Video Stream
    #cap = cv2.VideoCapture(0)
    #cap = VideoStream(scr=0).start()
    
    try:
        video_scr = video_scr[0]
    except:
        video_scr = 0
    
    cam = create_capture(video_scr)
    print('[INFO] Warming up camera...')
    # Time to warm up camera
    time.sleep(2)
    fps = FPS().start()

    # Loop over every frame
    while True:
        # Read the frame
        #ret, frame = cap.read()
        #frame = cap.read()
        _ret, frame = cam.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        
        # Resize the frame
        gray = imutils.resize(frame, width=min(350, frame.shape[1]))

        t = clock()

        # Detect people in frame
        (rects, weights) = hog.detectMultiScale(gray, winStride=(4, 4),
                padding=(8, 8), scale=1.10)
        print(weights)
        vis = gray.copy()
        # Apply non-maxima-suppression to amintain overlapping boxes
        rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
        pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

        # Draw the final boxes
        for (x1, y1, x2, y2) in pick:
            cv2.rectangle(vis, (x1, y1), (x2, y2), (0, 255, 0), 2)

        if not cascade.empty():
            for x1, y1, x2, y2 in rects:
                roi = gray[y1:y2, x1:x2]
                vis_roi = vis[y1:y2, x1:x2]
                subrects = detect(roi.copy(), cascade)
                draw_rects(vis_roi, subrects, (255, 0, 0))
                
        dt = clock() - t

        draw_str(vis, (20, 20), 'time: %.1f ms' % (dt*1000))
        cv2.imshow('Frame', vis)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        fps.update()
    #cap.release()
    #cap.stop()
    cv2.destroyAllWindows()

    fps.stop()
    print(fps.elapsed())
    print(fps.fps())
if __name__ == '__main__':
    main()
