from edgetpu.detection.engine import DetectionEngine
from edgetpu.utils import dataset_utils
from imutils.video import VideoStream
from imutils.video import FPS
from PIL import Image

import serial
import argparse
import cv2
import time
import imutils
import csv
import numpy as np
import datetime

# Local Libraries
from pyimagesearch.centroidtracker import CentroidTracker
from video import create_capture
from common import clock, draw_str

PORT = '/dev/serial0'
DETECTION_MODEL = r'./mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite'
FACE_MODEL = r'./data/haarcascades/haarcascade_frontalface_alt.xml'
LABEL = r'./coco_labels.txt'

def main():
    
    # Initialize centroid tracker and frame dimensions
    #ct = CentroidTracker()
    #(H, W) = (None, None)
    tracker = CentroidTracker(maxDisappeared=3, maxDistance=50)

    # Initialize engine
    print('[INFO]Loading model...')
    object_engine = DetectionEngine(DETECTION_MODEL)
    cascade = cv2.CascadeClassifier(FACE_MODEL)
    labels = dataset_utils.read_label_file(LABEL)

    # Initialize from serial port
    ser = serial.Serial(PORT, baudrate = 9600, timeout = 0.01)
    
    # Initialize time tracking
    object_id_list = []
    start_time = dict()
    end_time = dict()
    
    obj_dict = dict()

    data_from_GPS = ''
    speed_from_GPS = ''
    try:

        # Use cv2 to open video stream
        print('[INFO]Starting video stream...')
        #vs = VideoStream(scr=0).start()
        video_scr = 0
        
        cam = create_capture(video_scr)

        # Wait for camera sensor to warmup
        time.sleep(2.0)

        fps = FPS().start()

        # Create impressions csv file
        impressions_header = ['Id', 'GPS', 'Speed(mph)', 'Person', 
                'Ad Id', 'Ad Position', 'Start Time', 'End Time', 
                'Confidence %', 'Unit Id']
        impressions_writer = csv.writer(open(
            r'./CSV/Impressions ' + str(datetime.datetime.now())+'.csv', 'w'))
        impressions_writer.writerow(impressions_header)

        # Create timeline csv file
        timeline_header = ['Timestamp', 'GPS', 'Speed(mph)', 
                'Impression Present', 'Human in View']
        timeline_writer = csv.writer(open(
            r'./CSV/Timeline ' + str(datetime.datetime.now())+'.csv', 'w'))
        timeline_writer.writerow(timeline_header)

        while True:

            # Capture frame by frame
            _ret, frame = cam.read()
            
            # cv2.resize(frame,(width,height))
            # Display the frame
            # cv2.imshow('Video', frame)
            
            # Resize the frame
            frame = imutils.resize(frame, width=min(400, frame.shape[1]))
            #frame = vs.read()
            #frame = imutils.resize(frame, width=400)
            orig = frame.copy()

            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = cv2.equalizeHist(frame)
            
            frame = Image.fromarray(frame)

            t = clock()
            # Grab frame dimensions
            #if W is None and H is None:
            #    (H, W) = (400, 400) 

            # Run inference
            objs = object_engine.detect_with_image(frame, threshold=0.5,
                    keep_aspect_ratio=True, relative_coord=False, top_k=10)
            
            person_rects = []
            face_rects = []

            # Draw detected objects
            for obj in objs:
                label = labels[obj.label_id]
                if label == 'person':
                    # extract the bounding box and box and predicted class label
                    box = obj.bounding_box.flatten().astype("int")
                    (startX, startY, endX, endY) = box

                    person_rects.append(box)

                    # draw the bounding box and label on the image
                    cv2.rectangle(orig, (startX, startY), (endX, endY),
                                (0, 255, 0), 2)

            boundingboxes = np.array(person_rects)
            boundingboxes = boundingboxes.astype(int)
            person_rects = non_max_suppression_fast(boundingboxes, 0.3)
            
            for x1, y1, x2, y2 in person_rects:
                temp_roi = np.array(frame)
                roi = temp_roi[y1:y2, x1:x2]
                vis_roi = orig[y1:y2, x1:x2]
                subrects = detect(roi.copy(), cascade)

                #(subrects, rejectLevels, levelWeights) = cascade.detectMultiScale3(
                #        roi.copy(), scaleFactor=1.3, 
                #        minNeighbors=4, minSize=(30, 30), 
                #        flags=cv2.CASCADE_SCALE_IMAGE, outputRejectLevels=True)

                face_boxes = np.array(subrects)
                face_boxes = face_boxes.astype(int)
                subrects = non_max_suppression_fast(face_boxes, 0.3)
                draw_rects(vis_roi, subrects, (255, 0, 0))
                if len(subrects) != 0:
                    face_rects.append(subrects[0]) 

            # Update centroid tracker with the bouding box rectangles
            detected_objects = tracker.update(face_rects)

            # Read GPS data from serial port 
            GPS_data = ser.readline()
            print(GPS_data)
            try:
                # Decode byte data to string and split with ','
                data_from_GPS = get_data_from_GPS(
                        GPS_data.decode('utf-8').split(','))
                
                # Read speed data, decode and split
                speed_data = ser.readline()
                speed_from_GPS = get_speed_from_GPS(
                        speed_data.decode('utf-8').split(','))
            except(UnicodeDecodeError, AttributeError):
                pass
            
            if speed_from_GPS and data_from_GPS:
                if not detected_objects:
                    write_timeline_csv(objs, face_rects, data_from_GPS,
                        speed_from_GPS, timeline_writer)

            else:
                print('[INFO] No GPS')

            # Loop over tracked objects
            for (objectId, centroid) in detected_objects.items():
                (startX, startY, endX, endY) = centroid

                y = startY - 15 if startY - 15 > 15 else startY + 15
                
                # Calculate time in frame of object base on their ID
                if objectId not in object_id_list:
                    object_id_list.append(objectId)
                    start_time[objectId] = datetime.datetime.now()
                    end_time[objectId] = 0
                else:
                    curr_time = datetime.datetime.now()
                    old_time = start_time[objectId]
                    time_diff = curr_time - old_time
                    start_time[objectId] = datetime.datetime.now()
                    sec = time_diff.total_seconds()
                    end_time[objectId] += sec
                

            #    #text = "{}: {:.2f}% : ID {}".format(label, obj.score * 100,objectId)
                text = "ID {}, {} second".format(
                        objectId, int(end_time[objectId]))
                cv2.putText(orig, text, (startX, y),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                
                if data_from_GPS and speed_from_GPS:
                    write_impressions_csv(objectId, centroid, obj_dict,
                            data_from_GPS, speed_from_GPS,
                            start_time[objectId], end_time[objectId],
                            impressions_writer)

                    write_timeline_csv(objs, face_rects, data_from_GPS,
                            speed_from_GPS, timeline_writer)
            dt = clock() - t
            
            draw_str(orig, (20, 20), 'time: %.1f ms' % (dt*1000))
            # Show the video frame
            cv2.imshow('Frame', orig)

            # Exit video frame if press Q
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            fps.update()
    finally:
        fps.stop()
        print('[INFO] Elapsed time: {:.2f}'.format(fps.elapsed()))
        print('[INFO] Approx. FPS: {:.2f}'.format(fps.fps()))
        # Release the capture
        #vs.stop()
        cv2.destroyAllWindows()
        ser.close()

# Get data from GPS after split
def get_data_from_GPS(data):
    if data[0] == '$GPRMC':
        if data[2] == 'V':
            print('No satellite data')
            return
        
        latitude = convert_to_decimal_degree(data[3],data[4])
        longtitude = convert_to_decimal_degree(data[5],data[6])
        
        return latitude, longtitude

# Convert NMEA $GPRMC data to decimal degree
def convert_to_decimal_degree(coordinate, direction):
    multiplier = 1 if direction in ['N', 'E'] else -1
    
    degrees = float(coordinate) // 100
    minutes = float(coordinate) - 100*degrees

    return str(round(multiplier * (degrees + minutes/60),5))

# Get speed from GPS
def get_speed_from_GPS(data):
    if data[0] == '$GPVTG':
        return str(convert_speed_to_mph(float(data[7])))

# Convert to mph
def convert_speed_to_mph(kmh):
    return kmh/1.609

def detect(img, cascade):
    rects = cascade.detectMultiScale(img, scaleFactor=1.3, minNeighbors=4,
            minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    if len(rects) == 0:
        return []
    rects[:,2:] += rects[:,:2]
    return rects

def draw_rects(img, rects, color):
    for x1, y1, x2, y2 in rects:
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)

# Write to CSV
def write_impressions_csv(objId, face, obj_dict, data_from_GPS,
        speed_from_GPS, start_time, end_time, writer):

    if objId not in obj_dict:
        obj_dict[objId] = [objId,(data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Face Detected','1','Null', 
                start_time, end_time,'Null','1']

        if objId != 0:
            writer.writerow(obj_dict[objId - 1][-1][7])

    elif objId in obj_dict:
        end_time = start_time + datetime.timedelta(0, end_time)
        temp = [objId,(data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], 'Face Detected','1','Null', 
                start_time, end_time,'Null','1']
        obj_dict[objId].append(temp)

def write_timeline_csv(objs, faces, data_from_GPS, 
        speed_from_GPS, timeline_writer):
    if len(objs) > 0:
        temp = [datetime.datetime.now(), 
                (data_from_GPS[0] + ',' + data_from_GPS[1]),
                speed_from_GPS[0], len(faces), len(objs)]
        #time.sleep(1)
        timeline_writer.writerow(temp)

# Detect overlap boxes
def non_max_suppression_fast(boxes, overlapThresh):
    try:
        if len(boxes) == 0:
            return []

        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        pick = []

        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]

        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = np.argsort(y2)

        while len(idxs) > 0:
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            overlap = (w * h) / area[idxs[:last]]

            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > overlapThresh)[0])))

        return boxes[pick].astype("int")
    except Exception as e:
        print("Exception occurred in non_max_suppression : {}".format(e))

if __name__ == '__main__':
    main()

