## Pi libraries
```bash
sudo apt-get install libhdf5-dev libhdf5-serial-dev libhdf5-103
sudo apt-get install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
sudo apt-get install libatlas-base-dev
sudo apt-get install libjasper-dev
sudo apt-get install python3-edgetpu
sudo apt-get install python3-scipy
```

## Python3
```bash
pip3 install opencv-python==3.4.6.27
pip3 install matplotlib
pip3 install numpy
pip3 install imutils
pip3 install pynmea2
pip3 install omxplayer-wrapper
```
## Edge TPU library
1. Add package
```bash
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

sudo apt-get update
```
2. Install Edge TPU runtime:
**Pick one**
- Standard runtime
```bash
sudo apt-get install libedgetpu1-std
```
- Maximum runtime
```bash
sudo apt-get install libedgetpu1-max
```
### Camera Module
* Connecto to Camera Module Port
![](/images/camera1.png)
![](/images/camera2.gif)
* Startup your Pi
* Open Terminal type
![](/images/terminal.png)

* Go to Interfacing Options
![](/images/interface.png)

* Select Camera
![](/images/camerasetting.png)

* Select Yes to enabled
![](/images/cameraenable.png)

* After done, select Finish and reboot the Pi

## GPS Module
* Install the GPS Deamon
```bash
sudo apt-get install gpsd gpsd-clients
```
* Connect the GPS module to the Pi like the following
![](/images/gps.png)
* Open Terminal
```bash
sudo raspi-config
```

* Go to Interfacing Options
![](/images/interface.png)

* Select Serial
![](/images/serialsetting.png)

* Select No on enabled login shell
![](/images/loginshell.png)

* Select Yes to enabled serial port hardware
![](/images/serialport.png)

* Should get the following once completed
![](/images/serialfinal.png)

* After done, select Finish and reboot the Pi
* Open terminal and run
```bash
sudo killall gpsd
sudo gpsd /dev/serial0 -F /var/run/gpsd.sock
```
Then run this to test if the GPS working
```bash

cat /dev/serial0
```

or this to get the full stream
```bash

gpsmon /dev/serial0
```
