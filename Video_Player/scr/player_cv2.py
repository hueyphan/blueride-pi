import cv2
import numpy as np
from edgetpu.detection.engine import DetectionEngine
from imutils.video import VideoStream

def setup_video(filename):
    cap = cv2.VideoCapture(filename)

    if (cap.isOpened() == False):
        print('Error opening video')
        exit()

    #cv2.namedWindow('media', cv2.WND_PROP_FULLSCREEN)
    #cv2.setWindowProperty('media', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret:
            cv2.imshow('media', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        
        else:
            print('End video')
            break
    cap.release()
    cv2.destroyAllWindows()

def main():
    queue = []
    queue.append(r'./Videos/Dance.mp4')
    queue.append(r'./Videos/Dance2.mp4')

    for q in queue:
        setup_video(q)
        
if __name__ == '__main__':
    main()
