from edgetpu.detection.engine import DetectionEngine
from imutils.video import FileVideoStream
from imutils.video import FPS
import cv2
import numpy as np
import imutils
import time

def main():
    queue = []
    queue.append(r'../Videos/Dance.mp4')
    queue.append(r'../Videos/Dance2.mp4')

    for q in queue:
        video_player(q)

def video_player(filename):
    print('[INFO]Starting video file....')
    fvs = FileVideoStream(filename).start()
    time.sleep(1.0)
    
    while fvs.more():
        frame = fvs.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = np.dstack([frame, frame, frame]

        cv2.putText(frame, 'Queue Size: {}'.format(fvs.Q.qsize()),
                (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)

        cv2.imshow('Frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
    cv2.destroyAllWindows()
    fvs.stop()

if __name__ == '__main__':
    main()
