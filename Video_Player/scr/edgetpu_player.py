from edgetpu.classification.engine import ClassificationEngine
import cv2
import imutils
import numpy as np
import time

cap = cv2.VideoCapture(r'../Videos/Dance.mp4')
engine = ClassificationEngine('mobilenet_v2_1.0_224_quant_edgetpu.tflite')

while cap.isOpened():
    ret, frame = cap.read()
    #frame = cv2.resize(frame,(300, 300))
    objs = engine.classify_with_image(frame, threshold=0.5, top_k=3)
    cv2.imshow('frame', frame)

    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
